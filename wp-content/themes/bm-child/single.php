<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package benchmade
 */

get_header();
?>

    <div id="post-content" class="mr-auto ml-auto">
        <main id="main" class="main-content-area">

            <?php
            while ( have_posts() ) :
                the_post();
                get_template_part( 'template-parts/content', get_post_type() );

                the_post_navigation();

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;
                //Add related posts to the single post

            endwhile; // End of the loop.
            ?>

            <?php

            $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'orderby' => 'rand', 'numberposts' =>4, 'post__not_in' => array($post->ID) ) );
            if( $related ) ?>
                <?php
            echo do_shortcode('[smartslider3 slider="2"]');
            ?>
    <div class="related-articles flex">
        <h2 class="related-main-title">Related Articles</h2>
    <?php

                foreach( $related as $key =>  $post ) {
                setup_postdata($post); ?>
        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
                    <div class="related-container related-<?=$key?>" style="background-image: url(<?= $url ?>) " >

                        <a href="<?php the_permalink() ?>" class="related-title-articles"
                           rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?> </a>
                        <a href="<?php the_permalink() ?>" class="related-button" rel="bookmark" >Read More</a>
                       <div  class="related-article-image" rel="bookmark"   ""title="<?php the_title_attribute(); ?>"></div>
                    </li>
                </div>
            <?php }
            wp_reset_postdata(); ?>
            <div class="single-post-products font-gotham text-s  mb-5"><?php dynamic_sidebar( 'products_related_products' ); ?></div>
        </main><!-- #main -->
    </div><!-- #primary -->
    <div class="tailwind-class-generate related-0 related-1 related-2 related 3"></div><!-- just for gnerating tailwind classes -->
<?php
get_footer();
