<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package benchmade
 */

?>
<footer id="footer-main" class=" bg-black-500  text-white mr-auto md:ml-auto">

    <div class="inline-block lg:flex lg:flex-row w-full md:max-w-2xl justify-around lg:py-20 mr-auto md:ml-auto  sm:ml-10 sm:py-10 lg:pt-10">
        <div class=" inline-block lg:flex lg:flex-row justify-around bg-black-500 mx-auto md:ml-auto 2xl:min-w-2xl lg:min-w-lg md:min-w-md  font-gotham text-s mb-5 sm:inline-block w-full">
            <div class="font-gotham text-s  mb-5"> <?php dynamic_sidebar( 'footer_area_one' ); ?></div>
            <div class="font-gotham text-s  mb-5"><?php dynamic_sidebar( 'footer_area_two' ); ?></div>
            <div class="font-gotham text-s  mb-5"><?php dynamic_sidebar( 'footer_area_three' ); ?></div>
            <div class="font-gotham text-s  mb-5"><?php dynamic_sidebar( 'footer_area_four' ); ?></div>
        </div>
    </div>
    <div class="copyright-footer text-center font-gotham mx-5 pb-10 text-s">
        <?php dynamic_sidebar( 'copyright_section' ); ?>
    </div>
</footer><!-- #colophon -->
<?php wp_footer(); ?>