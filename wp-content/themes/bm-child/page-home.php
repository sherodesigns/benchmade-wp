<?php
/*
Template Name: Home
*/

get_header();
?>
    <section class="home-main">
        <div class="home-hero-image" style="background-image: url(<?= get_field ("hero_image") ?>)">
            <div class="hero-main-content">
          <div class="hero-reading-cat">
            <p class="home-reading-time">
             <svg xmlns="http://www.w3.org/2000/svg" class="timer-icon" fill="transparent" width="20px" height="20px" stroke="white" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path class="timer-icon" d="M272 0C289.7 0 304 14.33 304 32C304 49.67 289.7 64 272 64H256V98.45C293.5 104.2 327.7 120 355.7 143L377.4 121.4C389.9 108.9 410.1 108.9 422.6 121.4C435.1 133.9 435.1 154.1 422.6 166.6L398.5 190.8C419.7 223.3 432 262.2 432 304C432 418.9 338.9 512 224 512C109.1 512 16 418.9 16 304C16 200 92.32 113.8 192 98.45V64H176C158.3 64 144 49.67 144 32C144 14.33 158.3 0 176 0L272 0zM248 192C248 178.7 237.3 168 224 168C210.7 168 200 178.7 200 192V320C200 333.3 210.7 344 224 344C237.3 344 248 333.3 248 320V192z"/></svg>
              <?php echo get_field ("reading_time") ?></p>
            <p class="home-post-category ">
                <a href="<?php echo get_field ("category_url") ?>"><?php echo get_field ("post_category") ?></a></p>
          </div>
            <h1 class="home-hero-title"><?php echo get_field ("hero_title") ?></h1>
            <a href="<?= get_field ("hero_link") ?>" class="home-hero-btn">Read More</a>
        </div>
        </div>
        <div class="post-2-3-section flex flex-col  lg:flex-row justify-between mx-auto">
            <div class="post-2 w-2/3 lg:w-49.2 mt-10  mx-auto lg:mx-0 relative">
            <div class="post-home-image post-2-3-main post-container h-300 lg:h-670" style="background-image: url(<?= get_field ("post_2_image") ?>)">
                <div class="post-main-content font-gotham text-xs">
                <p class="post-reading-time flex">
                    <svg xmlns="http://www.w3.org/2000/svg" class="timer-icon" fill="transparent" width="20px" height="20px" stroke="white" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path class="timer-icon" d="M272 0C289.7 0 304 14.33 304 32C304 49.67 289.7 64 272 64H256V98.45C293.5 104.2 327.7 120 355.7 143L377.4 121.4C389.9 108.9 410.1 108.9 422.6 121.4C435.1 133.9 435.1 154.1 422.6 166.6L398.5 190.8C419.7 223.3 432 262.2 432 304C432 418.9 338.9 512 224 512C109.1 512 16 418.9 16 304C16 200 92.32 113.8 192 98.45V64H176C158.3 64 144 49.67 144 32C144 14.33 158.3 0 176 0L272 0zM248 192C248 178.7 237.3 168 224 168C210.7 168 200 178.7 200 192V320C200 333.3 210.7 344 224 344C237.3 344 248 333.3 248 320V192z"/></svg>
                    <?php echo get_field ("post_2_reading_time") ?></p>
                <p class="post-category pl-2.5 underline">
                    <a href="<?php echo get_field ("post_2_category_url") ?>"><?php echo get_field ("post_2_category") ?></a></p>
            </div>
                <h1 class="post-home-title"><?php echo get_field ("post_2_title") ?></h1>
                <a href="<?= get_field ("post_2_link") ?>" class="post-home-btn font-gotham-bold max-w-100 mx-auto">Read More</a>
               </div>
               </div>
            <div class="post-3 w-2/3 lg:w-49.2 my-10 mx-auto lg:mx-0 relative">
                <div class="post-home-image post-2-3-main post-container h-300 lg:h-670" style="background-image: url(<?= get_field ("post_3_image") ?>)">
                    <div class="post-main-content font-gotham text-xs">
                    <p class="post-reading-time flex">
                        <svg xmlns="http://www.w3.org/2000/svg" class="timer-icon" fill="transparent" width="20px" height="20px" stroke="white" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path class="timer-icon" d="M272 0C289.7 0 304 14.33 304 32C304 49.67 289.7 64 272 64H256V98.45C293.5 104.2 327.7 120 355.7 143L377.4 121.4C389.9 108.9 410.1 108.9 422.6 121.4C435.1 133.9 435.1 154.1 422.6 166.6L398.5 190.8C419.7 223.3 432 262.2 432 304C432 418.9 338.9 512 224 512C109.1 512 16 418.9 16 304C16 200 92.32 113.8 192 98.45V64H176C158.3 64 144 49.67 144 32C144 14.33 158.3 0 176 0L272 0zM248 192C248 178.7 237.3 168 224 168C210.7 168 200 178.7 200 192V320C200 333.3 210.7 344 224 344C237.3 344 248 333.3 248 320V192z"/></svg>
                        <?php echo get_field ("post_3_reading_time") ?></p>
                    <p class="post-category pl-2.5 underline">
                        <a href="<?php echo get_field ("post_3_category_url") ?>"><?php echo get_field ("post_3_category") ?></a></p>
                    </div>
                    <h2 class="post-home-title"><?php echo get_field ("post_3_title") ?></h2>
                    <a href="<?= get_field ("post_3_link") ?>" class="post-home-btn font-gotham-bold max-w-100 mx-auto">Read More</a>

                </div>
            </div>
            </div>

        <div class="post-4-container flex-col   mb-10 max-w-2xl mx-auto">
            <div class="post-4 w-full post-container relative">
                <div class="post-home-image post-4-main  h-300 lg:h-670" style="background-image: url(<?= get_field ("post_4_image") ?>)">
                    <div class="post-main-content font-gotham text-xs">
                        <p class="post-reading-time flex">
                            <svg xmlns="http://www.w3.org/2000/svg" class="timer-icon" fill="transparent" width="20px" height="20px" stroke="white" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path class="timer-icon" d="M272 0C289.7 0 304 14.33 304 32C304 49.67 289.7 64 272 64H256V98.45C293.5 104.2 327.7 120 355.7 143L377.4 121.4C389.9 108.9 410.1 108.9 422.6 121.4C435.1 133.9 435.1 154.1 422.6 166.6L398.5 190.8C419.7 223.3 432 262.2 432 304C432 418.9 338.9 512 224 512C109.1 512 16 418.9 16 304C16 200 92.32 113.8 192 98.45V64H176C158.3 64 144 49.67 144 32C144 14.33 158.3 0 176 0L272 0zM248 192C248 178.7 237.3 168 224 168C210.7 168 200 178.7 200 192V320C200 333.3 210.7 344 224 344C237.3 344 248 333.3 248 320V192z"/></svg>
                            <?php echo get_field ("post_4_reading_time") ?></p>
                        <p class="post-category pl-2.5 underline">
                            <a href="<?php echo get_field ("post_4_category_url") ?>"><?php echo get_field ("post_4_category") ?></a></p>
                    </div>
                    <h2 class="post-home-title latest-post-title"><?php echo get_field ("post_4_title") ?></h2>
                    <a href="<?= get_field ("post_4_link") ?>" class="post-home-btn text-center text-xs absolute inset-0 mx-auto font-gotham-medium  mx-auto">Read More</a>
                </div>
            </div>
        </div>
        <div class="post-5-6-7-section max-w-2xl flex flex-col lg:flex-row justify-between mx-auto">
            <div class="post-5 w-2/3 lg:w-32.3   mx-auto lg:mx-0 relative post-container">
                <div class="post-home-image post-5-6-7-main h-300 lg:h-670" style="background-image: url(<?= get_field ("post_5_image") ?>)">
                    <div class="post-main-content font-gotham text-xs">
                        <p class="post-reading-time flex">
                            <svg xmlns="http://www.w3.org/2000/svg" class="timer-icon" fill="transparent" width="20px" height="20px" stroke="white" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path class="timer-icon" d="M272 0C289.7 0 304 14.33 304 32C304 49.67 289.7 64 272 64H256V98.45C293.5 104.2 327.7 120 355.7 143L377.4 121.4C389.9 108.9 410.1 108.9 422.6 121.4C435.1 133.9 435.1 154.1 422.6 166.6L398.5 190.8C419.7 223.3 432 262.2 432 304C432 418.9 338.9 512 224 512C109.1 512 16 418.9 16 304C16 200 92.32 113.8 192 98.45V64H176C158.3 64 144 49.67 144 32C144 14.33 158.3 0 176 0L272 0zM248 192C248 178.7 237.3 168 224 168C210.7 168 200 178.7 200 192V320C200 333.3 210.7 344 224 344C237.3 344 248 333.3 248 320V192z"/></svg>
                            <?php echo get_field ("post_5_reading_time") ?></p>
                        <p class="post-category pl-2.5 underline">
                            <a href="<?php echo get_field ("post_5_category_url") ?>"><?php echo get_field ("post_5_category") ?></a></p>
                    </div>
                    <h2 class="post-home-title"><?php echo get_field ("post_5_title") ?></h2>
                    <a href="<?= get_field ("post_5_link") ?>" class="post-home-btn font-gotham-bold max-w-100 mx-auto">Read More</a>
                </div>
            </div>
            <div class="post-6 w-2/3 lg:w-32.3 mx-auto lg:mx-0 relative my-10 lg:my-0 ">
                <div class="post-home-image post-5-6-7-main h-300 lg:h-670 post-container" style="background-image: url(<?= get_field ("post_6_image") ?>)">
                    <div class="post-main-content font-gotham text-xs">
                        <p class="post-reading-time flex">
                            <svg xmlns="http://www.w3.org/2000/svg" class="timer-icon" fill="transparent" width="20px" height="20px" stroke="white" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path class="timer-icon" d="M272 0C289.7 0 304 14.33 304 32C304 49.67 289.7 64 272 64H256V98.45C293.5 104.2 327.7 120 355.7 143L377.4 121.4C389.9 108.9 410.1 108.9 422.6 121.4C435.1 133.9 435.1 154.1 422.6 166.6L398.5 190.8C419.7 223.3 432 262.2 432 304C432 418.9 338.9 512 224 512C109.1 512 16 418.9 16 304C16 200 92.32 113.8 192 98.45V64H176C158.3 64 144 49.67 144 32C144 14.33 158.3 0 176 0L272 0zM248 192C248 178.7 237.3 168 224 168C210.7 168 200 178.7 200 192V320C200 333.3 210.7 344 224 344C237.3 344 248 333.3 248 320V192z"/></svg>
                            <?php echo get_field ("post_6_reading_time") ?></p>
                        <p class="post-category pl-2.5 underline">
                            <a href="<?php echo get_field ("post_6_category_url") ?>"><?php echo get_field ("post_6_category") ?></a></p>
                    </div>
                    <h2 class="post-home-title"><?php echo get_field ("post_6_title") ?></h2>
                    <a href="<?= get_field ("post_6_link") ?>" class="post-home-btn font-gotham-bold max-w-100 mx-auto">Read More</a>

                </div>
            </div>
            <div class="post-7 w-2/3 lg:w-32.3 mx-auto lg:mx-0 relative ">
                <div class="post-home-image post-5-6-7-main h-300 lg:h-670 post-container" style="background-image: url(<?= get_field ("post_7_image") ?>)">
                    <div class="post-main-content font-gotham text-xs">
                        <p class="post-reading-time flex">
                            <svg xmlns="http://www.w3.org/2000/svg" class="timer-icon" fill="transparent" width="20px" height="20px" stroke="white" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path class="timer-icon" d="M272 0C289.7 0 304 14.33 304 32C304 49.67 289.7 64 272 64H256V98.45C293.5 104.2 327.7 120 355.7 143L377.4 121.4C389.9 108.9 410.1 108.9 422.6 121.4C435.1 133.9 435.1 154.1 422.6 166.6L398.5 190.8C419.7 223.3 432 262.2 432 304C432 418.9 338.9 512 224 512C109.1 512 16 418.9 16 304C16 200 92.32 113.8 192 98.45V64H176C158.3 64 144 49.67 144 32C144 14.33 158.3 0 176 0L272 0zM248 192C248 178.7 237.3 168 224 168C210.7 168 200 178.7 200 192V320C200 333.3 210.7 344 224 344C237.3 344 248 333.3 248 320V192z"/></svg>
                            <?php echo get_field ("post_7_reading_time") ?></p>
                        <p class="post-category pl-2.5 underline">
                            <a href="<?php echo get_field ("post_7_category_url") ?>"><?php echo get_field ("post_7_category") ?></a></p>
                    </div>
                    <h2 class="post-home-title"><?php echo get_field ("post_7_title") ?></h2>
                    <a href="<?= get_field ("post_7_link") ?>" class="post-home-btn font-gotham-bold max-w-100 mx-auto">Read More</a>

                </div>
            </div>
        </div>
        <div class="post-8 max-w-2xl my-10 mx-auto flex flex-col lg:flex-row relative  justify-between ">
            <div class="post-home-image post-5-6-7-main w-2/3 mx-auto lg:mx-0 lg:w-49.2 h-300 lg:h-670 post-container" style="background-image: url(<?= get_field ("post_8_image") ?>)">
            </div>
            <div class=" w-2/3 lg:w-49.2 h-250 lg:h-670 text-black mx-auto lg:mx-0 flex flex-col gap-8 lg:gap-16 justify-center items-center">
                <h2 class=" relative lg:absolute post-8-tittle font-gotham-bold text-28 lg:text-3xl text-black mb-0 lg:mb-20 post-home-title "><?php echo get_field ("post_8_title") ?></h2>
                <a href="<?= get_field ("post_8_link") ?>" class=" post-8-btn font-gotham-medium py-2 px-6 bg-blue text-white text-center hover:text-orange relative mt-4 lg:mt-20 lg:absolute">Read More</a>
            </div>
        </div>
        <div class="post-9-10-section max-w-2xl mx-auto flex flex-col lg:flex-row mb-0 lg:mb-10 justify-between mx-auto">
            <div class="post-9 w-2/3 lg:w-49.2 relative mx-auto lg:mx-0 post-container mb-10 lg:mb-0">
                <div class="post-home-image post-9-10-main  h-300 lg:h-670" style="background-image: url(<?= get_field ("post_9_image") ?>)">
                    <div class="post-main-content font-gotham text-xs ">
                        <p class="post-reading-time flex">
                            <svg xmlns="http://www.w3.org/2000/svg" class="timer-icon" fill="transparent" width="20px" height="20px" stroke="white" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path class="timer-icon" d="M272 0C289.7 0 304 14.33 304 32C304 49.67 289.7 64 272 64H256V98.45C293.5 104.2 327.7 120 355.7 143L377.4 121.4C389.9 108.9 410.1 108.9 422.6 121.4C435.1 133.9 435.1 154.1 422.6 166.6L398.5 190.8C419.7 223.3 432 262.2 432 304C432 418.9 338.9 512 224 512C109.1 512 16 418.9 16 304C16 200 92.32 113.8 192 98.45V64H176C158.3 64 144 49.67 144 32C144 14.33 158.3 0 176 0L272 0zM248 192C248 178.7 237.3 168 224 168C210.7 168 200 178.7 200 192V320C200 333.3 210.7 344 224 344C237.3 344 248 333.3 248 320V192z"/></svg>
                            <?php echo get_field ("post_9_reading_time") ?></p>
                        <p class="post-category pl-2.5 underline">
                            <a href="<?php echo get_field ("post_9_category_url") ?>"><?php echo get_field ("post_9_category") ?></a></p>
                    </div>
                    <h1 class="post-home-title"><?php echo get_field ("post_9_title") ?></h1>
                    <a href="<?= get_field ("post_9_link") ?>" class="post-home-btn font-gotham-bold max-w-100 mx-auto">Read More</a>
                </div>
            </div>
            <div class="post-10 w-2/3 lg:w-49.2 relative mx-auto lg:mx-0 post-container mb-10 lg:mb-0">
                <div class="post-home-image post-9-10-main h-300 lg:h-670" style="background-image: url(<?= get_field ("post_10_image") ?>)">
                    <div class="post-main-content font-gotham text-xs">
                        <p class="post-reading-time flex">
                            <svg xmlns="http://www.w3.org/2000/svg" class="timer-icon" fill="transparent" width="20px" height="20px" stroke="white" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path class="timer-icon" d="M272 0C289.7 0 304 14.33 304 32C304 49.67 289.7 64 272 64H256V98.45C293.5 104.2 327.7 120 355.7 143L377.4 121.4C389.9 108.9 410.1 108.9 422.6 121.4C435.1 133.9 435.1 154.1 422.6 166.6L398.5 190.8C419.7 223.3 432 262.2 432 304C432 418.9 338.9 512 224 512C109.1 512 16 418.9 16 304C16 200 92.32 113.8 192 98.45V64H176C158.3 64 144 49.67 144 32C144 14.33 158.3 0 176 0L272 0zM248 192C248 178.7 237.3 168 224 168C210.7 168 200 178.7 200 192V320C200 333.3 210.7 344 224 344C237.3 344 248 333.3 248 320V192z"/></svg>
                            <?php echo get_field ("post_10_reading_time") ?></p>
                        <p class="post-category pl-2.5 underline">
                            <a href="<?php echo get_field ("post_10_category_url") ?>"><?php echo get_field ("post_10_category") ?></a></p>
                    </div>
                    <h2 class="post-home-title"><?php echo get_field ("post_10_title") ?></h2>
                    <a href="<?= get_field ("post_10_link") ?>" class="post-home-btn font-gotham-bold max-w-100 mx-auto">Read More</a>

                </div>
            </div>
        </div>
        </div>
        <div class="home-take-quiz" >
            <div class="take-quiz-container" style="background-image: url(<?= the_field('q_image', $frontpage_id) ?>)">

                 <h3 class="quiz-subtitle"><?php echo get_field ("q_subtitle" ,$frontpage_id ) ?></h3>
                <h2 class="quiz-title"><?php echo get_field ("q_title" ,$frontpage_id ) ?></h2>
            <a class="quiz-btn" href="<?= get_field ("q_link", $frontpage_id) ?>">Take The Quiz</a>
            </div>
        </div>
        <div class="home-instagram-feed">
            <h4 class="instagram-title">@benchmadeknifecompany</h4>
            <h5 class="instagram-follow-link"> Follow us on <a href="https://instagram.com/benchmadeknifecompany?utm_medium=copy_link" target="_blank">Instagram</a></h5>
            <div id="instagram-feeds" class="<?= the_field('elfsight_instagram_key', $frontpage_id) ?>"></div>
        </div>
        <?php

    wp_reset_postdata(); ?>
        <div class="generate-tailwind-classes  copyright eapps-instagram-feed-posts-item-image-wrapper
       product-single-post eapps-instagram-feed-title  single-post-prod post-prod-title post-prod-btn"></div>
    </section>

<?php
get_footer();
