<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package benchmade
 */

get_header();
?>
    <section class="category-post-template">
        <?php if ( have_posts() ) : ?>
            <?php
            /* Start the Loop */
            $i = 1;
            while ( have_posts() ) :
                the_post();

                /*
                 * Include the Post-Type-specific template for the content.
                 * If you want to override this in a child theme, then include a file
                 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                 */
                ?>
                <article <?php echo $i % 2 === 0 ? 'class="category-posts"' : ''?>>

                    <?php get_template_part( 'template-parts/content', 'archive' ) ?>
                    <a class="category-read-more" href="<?php the_permalink(); ?>">Read More</a>
                </article>
                <?php echo $i % 2 === 0 ? '<div class="clear"></div>' : ''?>
                <?php
                $i++;
            endwhile;

            the_posts_navigation();

        else :

            get_template_part( 'template-parts/content', 'none' );

        endif;
        ?>

    </section>

<?php
get_footer();
