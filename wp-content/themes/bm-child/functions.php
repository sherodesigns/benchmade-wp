<?php
/**
 * benchmade-blog functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package benchmade
 */

if ( ! function_exists( 'bm_setup' ) ) :
    function bm_setup() {
    //Add Menu Locations on admin menu panel
        function register_my_menus() {
            register_nav_menus(
                array(
                    'menu-1' => __( 'Header' ),
                    'another-menu' => __( 'Another Menu' ),
                    'extra-menu' => __( 'Extra Menu' ),
                    'second-menu' => __( 'Second Menu' )
                )
            );
        }
        add_action( 'init', 'register_my_menus' );

        //Link JS files
        function bm_child_script(){
            wp_register_script('header-script', content_url(). '/themes/bm-child/js/main.js', array(), 1.0 , true);
            wp_enqueue_script('header-script');
        }
        add_action( 'wp_enqueue_scripts', 'bm_child_script' );

        //Link Css files
        function header_css(){
            wp_enqueue_style( 'bm-child-header', content_url(). '/themes/bm-child/header.css',array());
            wp_enqueue_style( 'bm-child-footer', content_url(). '/themes/bm-child/css/additional.css',array());
            wp_enqueue_style('bm-child_style', content_url(). '/themes/bm-child/public/output.css', array() );
        }
        add_action('get_header', 'header_css');

        add_filter('get_search_form', 'elevation_search_form');


        function elfsight_script() {
            wp_enqueue_script( 'elfsight',  'https://apps.elfsight.com/p/platform.js', array(), '20151215', true );

        }
        add_action( 'wp_enqueue_scripts', 'elfsight_script' );
        //Display the latest Posts on Homepage


function latest_post() {

    $args = array(
        'posts_per_page' => 9,
        'offset' => 0,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'post',
        'post_status' => 'publish'
    );
    $query = new WP_Query($args);
    if ($query->have_posts()) :
        $i= 0;

        while ($query->have_posts()) : $query->the_post();
        $i++;
            ?>

        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
            <?php
            if ($i==7){ ?>
                <div class="post-container latest-post-<?= $i ?>">
                  <div class="post-7-image" style="background-image: url(<?= $url ?>)"></div>
                    <div class="post-7-title">
                        <a class="title-post-7" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        <a class="post-7-btn" href="<?php the_permalink(); ?>">Read More</a>
                    </div>
                </div>
            <?php  } else{ ?>

            <div class="post-container latest-post-<?= $i ?>"style="background-image: url(<?= $url ?>) ">
           <div class="post-author-category">
               <p class="reading-time"><?php echo reading_time(); ?></p>
               <div class="latest-post-category"><?php the_category();?></p></div>
           </div>
            <a class="latest-post-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            <a class="post-read-more" href="<?php the_permalink(); ?>">Read More</a>
        </div>
            <?php
            }
        endwhile;
    endif;
}

add_shortcode('lastest-post', 'latest_post');

// Count reading time for each post
        function reading_time() {
            $content = get_post_field( 'post_content', $post->ID );
            $word_count = str_word_count( strip_tags( $content ) );
            $readingtime = ceil($word_count / 200);
            $icon = '<svg xmlns="http://www.w3.org/2000/svg" class="timer-icon" width="20px" height="20px" fill="transparent" stroke="white" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path class="timer-icon" d="M272 0C289.7 0 304 14.33 304 32C304 49.67 289.7 64 272 64H256V98.45C293.5 104.2 327.7 120 355.7 143L377.4 121.4C389.9 108.9 410.1 108.9 422.6 121.4C435.1 133.9 435.1 154.1 422.6 166.6L398.5 190.8C419.7 223.3 432 262.2 432 304C432 418.9 338.9 512 224 512C109.1 512 16 418.9 16 304C16 200 92.32 113.8 192 98.45V64H176C158.3 64 144 49.67 144 32C144 14.33 158.3 0 176 0L272 0zM248 192C248 178.7 237.3 168 224 168C210.7 168 200 178.7 200 192V320C200 333.3 210.7 344 224 344C237.3 344 248 333.3 248 320V192z"/></svg>';
            $totalreadingtime = $icon . $readingtime . 'min';
            return $totalreadingtime;
        }
        //Register Footer Widgets
        function register_widget_areas() {

            register_sidebar( array(
                'name'          => 'Footer area one',
                'id'            => 'footer_area_one',
                'description'   => 'This widget area discription',
                'before_widget' => '<div class="footer-columns title contact  lg:px-15 sm:px-5">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4 class=" text-s lg:text-l font-gotham-bold  sm:pb-0 sm:pt-5 font-bold mb-6 ">',
                'after_title'   => '</h4>',
            ));

            register_sidebar( array(
                'name'          => 'Footer area two',
                'id'            => 'footer_area_two',
                'description'   => 'This widget area discription',
                'before_widget' => '<div class="footer-columns title lg:px-15 sm:px-5">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4 class=" text-s lg:text-l font-gotham-bold sm:pb-0 sm:pt-5 font-bold mb-6 ">',
                'after_title'   => '</h4>',
            ));

            register_sidebar( array(
                'name'          => 'Footer area three',
                'id'            => 'footer_area_three',
                'description'   => 'This widget area discription',
                'before_widget' => '<div class="footer-columns title lg:px-15 sm:px-5">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4 class=" text-s lg:text-l font-gotham-bold sm:pb-0 sm:pt-5 font-bold mb-6 ">',
                'after_title'   => '</h4>',
            ));

            register_sidebar( array(
                'name'          => 'Footer area four',
                'id'            => 'footer_area_four',
                'description'   => 'This widget area discription',
                'before_widget' => '<div class="footer-columns title lg:px-15 sm:px-5">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4 class=" text-s lg:text-l font-gotham-bold  sm:pb-0 sm:pt-5 font-bold mb-6 ">',
                'after_title'   => '</h4>',
            ));
            register_sidebar( array(
                'name'          => 'Copyright',
                'id'            => 'copyright_section',
                'description'   => 'This widget area discription',
                'before_widget' => '<div class="md:px-20 sm:px-5  text-center">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4 class="text-l pb-2">',
                'after_title'   => '</h4>',
            ));
            register_sidebar( array(
                'name'          => 'Products Related Products',
                'id'            => 'products_related_products',
                'description'   => 'This widget area discription',
                'before_widget' => '<div class="">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4 class="">',
                'after_title'   => '</h4>',
            ));
            register_sidebar( array(
                'name'          => 'Header Second Menu Title',
                'id'            => 'header_second_title',
                'description'   => 'This widget area discription',
                'before_widget' => '<div class="">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4 class="">',
                'after_title'   => '</h4>',
            ));
        }
        add_theme_support( 'post-thumbnails' );
        add_action( 'widgets_init', 'register_widget_areas' );
        function elevation_search_form($form) {
            $form = '<form action="' . esc_url( home_url( '/' ) ) . '" method="get" class="search-form" onsubmit="searchButtonClick()">
       <button type="button" id="search-submit" onclick="searchButtonClick()" class="search-submit">
       <svg width="18px" height="19px" viewBox="0 0 20 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <title>Search</title>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="search-icon" transform="translate(-100.000000, -3687.000000)" fill="#000000" fill-rule="nonzero">
                                <g id="Seach" transform="translate(100.000000, 3687.000000)">
                                    <path d="M18.6327681,20.499994 C18.7629759,20.499994 18.8801631,20.447911 18.9843297,20.3437444 L18.9843297,20.3437444 L19.8827651,19.4843715 C19.9608899,19.3802048 19.9999523,19.2630176 19.9999523,19.1328098 C19.9999523,19.002602 19.9608899,18.8984354 19.8827651,18.8203105 L19.8827651,18.8203105 L15.1171515,14.0546969 C15.0390266,13.9765721 14.93486,13.9375097 14.8046522,13.9375097 L14.8046522,13.9375097 L14.2577785,13.9375097 C14.882777,13.2083446 15.3710571,12.388034 15.7226188,11.4765781 C16.0741804,10.5651221 16.2499613,9.61460313 16.2499613,8.62502235 C16.2499613,7.14065089 15.885379,5.77997725 15.1562139,4.54300083 C14.4270487,3.30602442 13.4439786,2.32295424 12.2070021,1.59378912 C10.9700257,0.864623989 9.60935209,0.500041723 8.12498063,0.500041723 C6.64060917,0.500041723 5.27993553,0.864623989 4.04295911,1.59378912 C2.80598269,2.32295424 1.82291252,3.30602442 1.09374739,4.54300083 C0.364582265,5.77997725 0,7.14065089 0,8.62502235 C0,10.1093938 0.364582265,11.4700675 1.09374739,12.7070439 C1.82291252,13.9440203 2.80598269,14.9270905 4.04295911,15.6562556 C5.27993553,16.3854207 6.64060917,16.750003 8.12498063,16.750003 C9.1145614,16.750003 10.0650804,16.5742221 10.9765363,16.2226605 C11.8879923,15.8710988 12.7083028,15.3828187 13.437468,14.7578202 L13.437468,14.7578202 L13.437468,15.3046939 C13.437468,15.4088605 13.4765304,15.5130266 13.5546552,15.6171932 L13.5546552,15.6171932 L18.3202688,20.3437444 C18.3983936,20.447911 18.5025603,20.499994 18.6327681,20.499994 Z M8.12498063,14.8750075 C7.00519143,14.8750075 5.96352765,14.59506 4.99998808,14.0351657 C4.03644851,13.4752714 3.27473158,12.7135545 2.71483728,11.7500149 C2.15494298,10.7864753 1.87499553,9.74481155 1.87499553,8.62502235 C1.87499553,7.50523316 2.15494298,6.46356937 2.71483728,5.5000298 C3.27473158,4.53649023 4.03644851,3.7747733 4.99998808,3.214879 C5.96352765,2.6549847 7.00519143,2.37503725 8.12498063,2.37503725 C9.24476982,2.37503725 10.2864336,2.6549847 11.2499732,3.214879 C12.2135127,3.7747733 12.9752297,4.53649023 13.535124,5.5000298 C14.0950183,6.46356937 14.3749657,7.50523316 14.3749657,8.62502235 C14.3749657,9.74481155 14.0950183,10.7864753 13.535124,11.7500149 C12.9752297,12.7135545 12.2135127,13.4752714 11.2499732,14.0351657 C10.2864336,14.59506 9.24476982,14.8750075 8.12498063,14.8750075 Z" id="Search"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
        </button>
        <input type="text" name="search-input" id="input-btn-child" class="search-bm-input"  placeholder="Search by name, category, sku.." value="' . esc_attr( get_search_query() ) . '" required>
        
        </form>';
            return $form;
        }


    }

endif;
add_action( 'after_setup_theme', 'bm_setup' );
?>