/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */

// Header hamburger menu script
document.getElementById('hamburger-mobile').addEventListener('click', function (){
    document.querySelector('body').classList.toggle("mobile-menu-open")
});
document.getElementById('hamburger-svg-icon').addEventListener('click', function (){
    document.querySelector('body').classList.remove("mobile-menu-open")
});
// Append the "Find A Dealer" to the menu last-child
function appendFindDealer(){
    if (window.innerWidth<= 1024) {
        const node = document.createElement("li");
        const appendMenu = document.getElementById('find-dealer')
        const primarymenu = document.getElementById('primary-menu')
        node.appendChild(appendMenu);
        primarymenu.append(node);
    }
}
appendFindDealer()


//Footer Dropdownd Scripts
const allFooterItems = Array.from(document.querySelectorAll('footer h4'));
allFooterItems.forEach(element=>{
    element.addEventListener('click',function(){
        element.parentNode.classList.add('active');
        const index = allFooterItems.indexOf(element);
        const result = allFooterItems.filter( element => element !== allFooterItems[index]);
        result.forEach(items => {
            items.classList.remove('active');
            items.parentNode.classList.remove('active');
        })
    })
})
/// Redirect the Search Input to Magento Site Results
const searchInput = document.getElementById("input-btn-child");
const mobileInput =  document.querySelector('#mobile-search #input-btn-child');
function searchButtonClick() {
   const mobile =  document.querySelector('#mobile-search #input-btn-child').value;
   const desktop =  document.querySelector('#search-desktop #input-btn-child').value;
    if (window.innerWidth< 768) {
            window.location.replace(`https://www.benchmade.com/catalogsearch/result/?q=${mobile}`);
        }else{
            window.location.replace(`https://www.benchmade.com/catalogsearch/result/?q=${desktop}`);
        }
};
let searchInputForm = searchInput;
if (window.innerWidth< 768) {
    searchInputForm=mobileInput ;
}
searchInputForm?.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13 || event.keyCode === 10) {

        searchButtonClick()
    }
});


