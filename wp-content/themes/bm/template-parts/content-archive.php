<a href="<?php the_permalink(); ?>" title="Read more">
    <?php the_post_thumbnail() ?>
    <h2 class="title"><?php the_title(); ?></h2>
</a>

<div class="posted-on"><?php echo get_the_date() ?></div>
<div class="excerpt">
    <?php the_excerpt(); ?>
</div>
<div class="footer">
    <ul class="categories">
        <?php foreach (get_the_category() as $category) : ?>
            <li><a href="/category/<?php echo $category->slug ?>"><?php echo $category->cat_name ?></a></li>
        <?php endforeach; ?>
    </ul>
    <a href="<?php the_permalink() ?>#comments" class="comment-count"><?php comments_number( '0', '1', '%'); ?><span class="fas fa-comments"></span></a>
</div>