<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package benchmade
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bm' ); ?></a>

	<header id="masthead" class="site-header">
        <div class="content-area">
            <div class="site-branding">
                <a href="https://www.benchmade.com"><img class="logo" src="<?php echo get_template_directory_uri() ?>/images/logo.svg" title="Benchmade" alt="Benchmade" /></a>
                <?php
                $bm_description = get_bloginfo( 'description', 'display' );
                if ( $bm_description || is_customize_preview() ) :
                    ?>
                    <p class="site-description"><?php echo $bm_description; /* WPCS: xss ok. */ ?></p>
                <?php endif; ?>
            </div><!-- .site-branding -->
            <div class="blog-logo">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/images/blog-logo.png" title="Benchmade Blog" alt="Benchmade Blog" />
                </a>
            </div>
            <nav id="site-navigation" class="main-navigation">
                <div class="hamburger-menu">
                    <a href="#" class="hamburger"><span>Sections</span></a>
                </div>
                <button class="search" type="button"></button>
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1',
                        'menu_id'        => 'primary-menu',
                    ) );
                ?>
                <div class="search-menu">
                    <?php get_search_form(); ?>
                </div>
            </nav><!-- #site-navigation -->
        </div>
	</header><!-- #masthead -->

    <?php echo bm_display_hero_image() ?>

	<div id="content" class="site-content">
