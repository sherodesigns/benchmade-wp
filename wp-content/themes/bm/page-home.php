<?php
/*
Template Name: Home
*/

get_header();
?>
    <section class="grid content-area">

        <?php
        $paged = get_query_var('page') ? get_query_var('page') : 1;
        $args = array(
            'posts_per_page' => 4,
            'paged' => $paged
        );

        $wp_query = new WP_Query($args);
        $i = 1;
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <article <?php echo $i % 2 === 0 ? 'class="even"' : ''?>>
                <?php get_template_part( 'template-parts/content', 'archive' ) ?>
            </article>
            <?php echo $i % 2 === 0 ? '<div class="clear"></div>' : ''?>
        <?php $i++; ?>
        <?php endwhile; ?>

        <?php if ($paged > 1) { ?>

            <nav id="nav-posts">
                <div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
                <div class="next"><?php previous_posts_link('Newer Posts &raquo;'); ?></div>
            </nav>

        <?php } else { ?>

            <nav id="nav-posts">
                <div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
            </nav>

        <?php } ?>

        <?php wp_reset_postdata(); ?>

    </section>

<?php
get_footer();
