<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package benchmade
 */

get_header();
?>

    <section class="grid content-area">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'bm' ), '<span>' . get_search_query() . '</span>' );
					?>
				</h1>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
            $i = 1;
			while ( have_posts() ) :
				the_post();
                ?>
                <article <?php echo $i % 2 === 0 ? 'class="even"' : ''?>>
                    <?php get_template_part( 'template-parts/content', 'archive' ) ?>
                </article>
                <?php
                echo $i % 2 === 0 ? '<div class="clear"></div>' : '';
                $i++;
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</section><!-- #primary -->

<?php
get_footer();
