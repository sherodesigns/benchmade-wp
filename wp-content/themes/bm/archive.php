<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package benchmade
 */

get_header();
?>
    <section class="grid content-area">

		<?php if ( have_posts() ) : ?>
			<?php
			/* Start the Loop */
            $i = 1;
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				?>
                <article <?php echo $i % 2 === 0 ? 'class="even"' : ''?>>
                    <?php get_template_part( 'template-parts/content', 'archive' ) ?>
                </article>
                <?php echo $i % 2 === 0 ? '<div class="clear"></div>' : ''?>
            <?php
                $i++;
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

    </section>

<?php
get_footer();
