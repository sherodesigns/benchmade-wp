jQuery(document).ready(function(){
    jQuery('.mobile-navigation .footer-block-heading').on('click', function(){
        var content = jQuery(this).closest('.mobile-navigation-section').find('.block-content');
        jQuery(this).toggleClass('activeTab');
        if (jQuery(this).hasClass('activeTab')) {
            content.slideDown();
        } else {
            content.slideUp();
        }
    });
    jQuery('.main-navigation .hamburger').on('click', function(){
        var menu = jQuery(this).closest('.main-navigation');
        menu.toggleClass('open-menu').removeClass('open-search');
        if (menu.hasClass('open-menu')) {
            jQuery('.hamburger span').text('Close');
        } else {
            jQuery('.hamburger span').text('Sections');
        }
    });

    jQuery('.main-navigation .search').on('click', function(){
        var menu = jQuery(this).closest('.main-navigation');
        menu.toggleClass('open-search').removeClass('open-menu');
        if (menu.hasClass('open-search')) {
            menu.find('.search-field').focus();
        }
    });
});