<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package benchmade
 */

?>

	</div><!-- #content -->


	<footer id="colophon" class="site-footer">
		<div class="content-area">
            <div class="mobile-navigation">
                <div class="mobile-navigation-section">
                    <h3 class="footer-block-heading">CONNECT</h3>
                    <div class="social-icons block-content">
                        <a href="https://www.facebook.com/Benchmade" class="icon-facebook" title="Facebook" target="_blank">Facebook</a>
                        <a href="https://twitter.com/BenchmadeKnives" class="icon-twitter" title="Twitter" target="_blank">Twitter</a>
                        <a href="https://www.instagram.com/benchmadeknifecompany/" class="icon-insta" title="Instagram" target="_blank">Instagram</a>
                    </div>
                </div>
            </div>
            <div class="footer-columns">
                <div class="footer-logo"><img src="<?php echo get_template_directory_uri() ?>/images/footer-logo.png" alt="Benchmade"></div>
                <div class="address-block">
                    <a class="telephone" href="tel:1-800-800-7427">1-800-800-7427</a><br>
                    <address class="address">
                        300 BeaverCreek Road<br>
                        Oregon City, OR 97045
                    </address>
                </div>
            </div>
            <div class="footer-columns social-icon-section">
                <h3 class="footer-block-heading">CONNECT</h3>
                <div class="social-icons block-content">
                    <a href="https://www.facebook.com/Benchmade" class="icon-facebook" title="Facebook" target="_blank">Facebook</a>
                    <a href="https://twitter.com/BenchmadeKnives" class="icon-twitter" title="Twitter" target="_blank">Twitter</a>
                    <a href="https://www.instagram.com/benchmadeknifecompany/" class="icon-insta" title="Instagram" target="_blank">Instagram</a>
                    <p class="benchmade-forum">BENCHMADE FORUM</p>
                </div>
                <div class="privacy-policy">
                <?php
                    wp_nav_menu( array(
                        'menu'        => 'footer',
                    ) );
                ?>
                </div>
                <small class="copyright">&copy; <?php echo date("Y") ?> Benchmade, Inc. All Rights Reserved</small>
            </div>
		</div>
	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
