<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              www.perficientdigital.com
 * @since             1.0.0
 * @package           Benchmade_Blog_Google_Analytics
 *
 * @wordpress-plugin
 * Plugin Name:       Benchmade Blog Google Analytics
 * Plugin URI:        https://blog.benchmade.com
 * Description:       Benchmade Blog Analytics Plugin by Perficient
 * Version:           1.0.0
 * Author:            Imran Virk
 * Author URI:        www.perficientdigital.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       benchmade-blog-google-analytics
 * Domain Path:       /languages
 */

function benchmade_google_analytics() { ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-67034720-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-67034720-1');

        function getCookiesMap(cookiesString) {
            return cookiesString.split(";")
                .map(function (cookieString) {
                    return cookieString.trim().split("=");
                })
                .reduce(function (acc, curr) {
                    acc[curr[0]] = curr[1];
                    return acc;
                }, {});
        }
        var cookies = getCookiesMap(document.cookie);
        var cookieUserID = cookies["STSID732309"]
        console.log(cookieUserID);

        gtag('set', {
            'user_id': cookieUserID
        }); // Set the user ID using STSID732309 cookie value.
        ga('set', 'userId', cookieUserID); // Set the user ID using STSID732309 cookie value
    </script>

<?php }
add_action( 'wp_head', 'benchmade_google_analytics', 10 );
