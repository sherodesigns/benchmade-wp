=== Plugin Name ===
Contributors: Imran Virk <imran.virk@perficient.com>
Link: www.perficientdigital.com
Tags: Benchmade, Blog, Analytics
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Benchmade Blog Analytics Plugin by Perficient

== Description ==

Benchmade Blog Analytics Plugin by Imran Virk at Perficient <imran.virk@perficient.com>

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `benchmade-blog-google-analytics.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Place `<?php do_action('plugin_name_hook'); ?>` in your templates

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

= 1.0 =
* Intitial plugin development release

== Upgrade Notice ==

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.